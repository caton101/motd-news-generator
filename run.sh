#!/bin/sh

# MOTD News Generator
# Copyright (C) 2022 Cameron Himes
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

# settings
REPO_DIR=/root/motd-news-generator
WEB_DIR=/var/www/localhost
FILE_NAME=index.html

# ensure operations are inside repo dir
cd $REPO_DIR

# refresh repo
git -C $REPO_DIR pull

# generate file target path
FILE_PATH="$WEB_DIR/$FILE_NAME"

# execute script
$REPO_DIR/gen.sh $FILE_PATH

# set ownership
chown root:www-data $FILE_PATH
