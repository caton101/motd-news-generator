# MOTD News Generator

This is a tool that pipes random lines of text through cowsay and serves it on
the web as an MOTD server. It is designed to be used with Ubuntu's `motd-news`
package.

## Installation

### On the server

**NOTE**: The `run.sh` file was designed for Alpine Linux and may need changes.

1. Install a web server
2. Install `cowsay`
3. Clone this repo somewhere
4. Change the file paths in `run.sh` to match your configuration
5. Edit the `lines.txt` file to include any desired text
6. Schedule a crontab to execute `run.sh` at any interval (1 minute recommended)
7. Edit DNS so that `motd.example.com` shows the MOTD

### On the client

According to [this link](https://news.ycombinator.com/item?id=14663947), the
configuration file for the `motd-news` package is at ` /etc/default/motd-news`.

1. Open ` /etc/default/motd-news` in your preferred editor
2. Change the `URLS="https://motd.ubuntu.com"` line to say
   `URLS="https://motd.example.com"`

## Configuration Info

### In gen.sh

- `LINES_FILE` contains the file with text lines that can be randomly chosen and
  piped through cowsay
  - empty lines are ignored
  - any lines prefaced with `#` are ignored
- `DESTINATION_FILE` defines where the new MOTD file is saved

**NOTE**: This script uses *any* cow file. This means some cows may be
inappropriate. This can be fixed by removing any inappropriate cow files or
using a custom file path with preselected cow files.

### In run.sh

- `REPO_DIR` contains the file path to where this git repository is cloned
- `WEB_DIR` contains the file path to where the web server is storing webpages
- `FILE_NAME` contains the name of the file where the MOTD will be stored

