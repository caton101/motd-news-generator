#!/bin/sh

# MOTD News Generator
# Copyright (C) 2022 Cameron Himes
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

# settings
LINES_FILE="lines.txt"
DESTINATION_FILE="out.txt"

# check args
if [ $# -ge 1 ]
then
    DESTINATION_FILE="$1"
fi

# set up container to store line
LINE=""

# keep getting lines until a suitable one is found
until  [ -n "$LINE" -a "${LINE:0:1}" != "#" ]
do
    # get a line to use
    LINE="$(shuf -n 1 $LINES_FILE)"
done

# run through cowsay
LINE="$(cowsay -f $(ls $(cowsay -l | head -n 1 | sed -e 's+Cow files in ++g' | sed 's+:++g')/*.cow | shuf -n 1) $LINE)"

# save to file
echo "$LINE" > "$DESTINATION_FILE"
